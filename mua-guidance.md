---
title: Guidance for Implementers of Mail User Agents
docname: draft-dkg-emailcore-mua-guidance-00
date: 2020-11-15
category: info

ipr: trust200902
area: int
workgroup: emailcore
keyword: Internet-Draft

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
  ins: D. K. Gillmor
  name: Daniel Kahn Gillmor
  org: American Civil Liberties Union
  street: 125 Broad St.
  city: New York, NY
  code: 10004
  country: USA
  abbrev: ACLU
  email: dkg@fifthhorseman.net
informative:
  RFC1939:
  RFC2045:
  RFC3501:
  RFC5228:
  RFC5321:
  RFC5322:
  RFC6409:
  RFC8620:
normative:
  RFC2119:
  RFC8174:

--- abstract

Writing a Mail User Agent (an e-mail client) is a complex task.
This document provides guidance for anyone implementing a MUA. 

--- middle

Introduction
============

A Mail User Agent (MUA) is a piece of software that enables the user to send, receive, search, review, and manipulate e-mail messages.

This draft focuses on MUAs that use standard Internet protocols (such as SMTP [RFC5321], Submission [RFC6409],  IMAP [RFC3501], JMAP [RFC8620], POP [RFC1939], and SIEVE [RFC5228) and storage formats (e.g., Internet Message Format [RFC5322] and MIME [RFC2045]).
But it also acknowledges that most MUAs also need additional, non-standardized tooling to enable functionality that e-mail users have come to expect.

It documents subtle nuances related to wire protcols and storage formats, and also summarizes common user expectations and their implications for how to craft a simple and effective user experience.

Requirements Language
---------------------

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in BCP 14 ([RFC2119] and [RFC8174]) when, and only when, they appear in all capitals, as shown here.

Terminology
-----------

For the purposes of this document, we try to use the following words with fairly pedantic definitions:

- *MUA* is short for Mail User Agent; an e-mail client
- *Server* refers to any network server that provides e-mail services
- *Message* is an e-mail message [RFC5322], including messages that have not been sent yet
- *Address* is an e-mail address as specified in [RFC5321], subtly different from…
- *Account* is the set of authorization and authentication parameters that a *MUA* uses to interact with any e-mail server.
- *Folder* is a collection of Messages, accessible to the MUA either locally (in the filesystem) or via connection to a Server via an Account, using some protocol like IMAP [RFC3501] or JMAP [RFC8620].
- *Peer MUA* is the Mail User Agent on the other end of a communication.
  When the local MUA is composing a message, the Peer MUA is the MUA that receives and processes the message.
  When the local MUA has received a message, the Peer MUA is the MUA that composed the message and injected it into the Internet e-mail system.
- *Mail Submission Agent* is the first hop relay that the MUA hands off a message to after composition, typically via Submission [RFC6409] or JMAP [RFC8620].
- *Mail Transport Agent* is a server that transfers messages to other Mail Transport Agents, typically via SMTP [RFC5321].
- a *Linked MUA* is another MUA that uses the same *Account* as the local MUA.

Sent Mail
=========

TODO: discuss how stored Sent Mail varies from stored received mail, including at least:

- Bcc headers might be stored in a "Sent" message (but might not)
- Copies stored at send time (e.g. Fcc) have no headers added by the transport system

Drafts Folder
=============

TODO: discuss details about the "drafts" folder, including at least:

- communication between two different linked MUAs (or the same MUA over time/version)
- stashing message composition state that might not be appropriate for the wire format

Headers
=======

We document some guidance and insight about certain headers and their behavior.

Message-ID
----------

TODO: guidance about the Message-ID header, including:

- when generating a message, the MUA SHOULD select a Message-ID, rather than allowing the mail transport system to inject its own Message-ID.
- Message-ID selection SHOULD NOT leak local implementation details
- recommendation for simple, anonymized Message-ID selection?
 
User-Agent
----------

The composing MUA SHOULD NOT include a User-Agent header, to avoid leaking details that could be useful for an attacker to target the user.


Date
----

TODO: should the `Date` header be formatted in UTC or local time?

Bcc
---

TODO: refer to notes in Drafts and Sent Mail sections above.  see also [RFC5322], § 3.6.3

Auto-Configuration
==================

Users don't want to spend time configuring (or understanding how to configure) their MUA.
In general, the user wants a configuration where they enter their e-mail address and password and that's it.
Alternately, some sophisticated users might want to plug in a smartcard and have the MUA autoconfigure its account appropriately.

TODO: describe or point to the most popular autoconfiguration mechanisms.


IANA Considerations
===================

Nothing for IANA to do in this draft.

Security Considerations
=======================

- consider referencing draft on e2e crypto for MUAs
- Message-ID and User-Agent guidance has security implications.

Document Considerations
=======================

\[ RFC Editor: please remove this section before publication ]

This document is currently edited as markdown.
Minor editorial changes can be suggested via merge requests at https://gitlab.com/dkg/mua-guidance or by e-mail to the author.
Please direct all significant commentary to the public IETF EMAILCORE mailing list: emailcore@ietf.org

Document History
----------------

Acknowledgements
================

The set of constructs and recommendations in this document are derived from discussions with many different implementers, including
Alexey Melnikov and
Bernie Hoeneisen.

--- back

Test Vectors
============

FIXME: This document should contain examples of well-formed e-mail messages and transcripts of common wire protocol interactions.

It may also include example renderings of the sample messages.
