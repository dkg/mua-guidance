#!/usr/bin/make -f

draft = mua-guidance
OUTPUT = $(draft).txt $(draft).html $(draft).xml $(draft).pdf
all: $(OUTPUT)

%.xmlv2: %.md
	kramdown-rfc2629 < $< > $@.tmp
	mv $@.tmp $@

%.xml: %.xmlv2
	xml2rfc --v2v3 -o $@ $<

%.html: %.xml
	xml2rfc $< --html

%.pdf: %.xml
	xml2rfc $< --pdf

%.txt: %.xml
	xml2rfc $< --text

clean:
	-rm -rf $(OUTPUT) metadata.min.js *.tmp

.PHONY: clean all
